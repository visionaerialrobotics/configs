| Folder | Vehicle            |  Mission |
|:------ |:------------------ |:-------- |
| drone0 | Asctec Pelican     |          |
| drone1 | AR Drone 2.0       | Squared trajectory |
| drone2 | Oktokopter         |          |
| drone3 | Pixhawk            |          |
| drone4 | Bebop              |          |
| drone5 | Pixhawk simulation |          |
| drone6 | DJI                |          |
| drone7 | Rotors simulation  |  Swarm   |
| drone11 | Rotors simulation  |  Swarm   |
